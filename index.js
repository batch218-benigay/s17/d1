//console.log("Hello World!");

/* [SECTION] FUNCTIONS IN JAVASCRIPT are lines/block of codes that tell our device application to perform a certain task when called/invoke.
	-Also used to prevent repeating lines/blovk of codes that perform the same task/function.*/

/* Function Declaration
	Function statement defines a function with parameters*/

	//FUNCTION KEYWORD -  used to devine JavaScript function
	// FUNCTION NAME - used so we are able to call/invoke a declared function
	// FUNCTION CODE BLOCK ({ }) - the statement which compromise the body of the function. This is where the code to be executed.

	//function declaration // function name
	//function name requires open and close parenthesis beside it

	console.log("FUNCTION DECLARATION");
	function printName(){ //code block
		console.log("My name is John"); // function statement
	}; //delimeter

	printName();

	/* [HOISTING]
		-is JavaScript's behavior for certain variable and functions to run or use*/

	console.log("------------------------");
	console.log("HOISTING");
	declaredFunction(); // not defined
	// Make sure the function is existing whenever we call/invoke a function


	function declaredFunction(){
		console.log("Hello World");
	};

	/*[FUNCTION EXPRESSION]
		-a function can also be stored in a variable called as functions expression.
		- Hoisting is allowed in function declaration, however, we could not hoist through function expression. */

		console.log("------------------------");
		console.log("FUNCTION EXRESSION");
		let variableFunction = function(){
			console.log("Hello again!");
		};

		variableFunction();

		let funcExpression = function funcName(){
			console.log("Hello from the other the side");
		};

		funcExpression ();
		//funcName(); - Error / when calling a named function stored in a variable, just use the variable name, not the function name.

console.log("------------------------");
console.log("REASSIGNING FUNCTION");

declaredFunction();

declaredFunction = function(){
	console.log("Updated declaredFunction");
};

declaredFunction();

funcExpression = function(){
	console.log("Updated funcExpression");
};

funcExpression();

/*CONSTANT FUNCTION
-The accessibility / visibility of a variables in the code */
	console.log("------------------------");
	console.log("CONSTANT FUNCTION");

	const constantFunction = function(){
		console.log("Initialize with const.");
	};

	constantFunction();

	/*constantFunction = function () {
		console.log("New value");
	};
	constantFunction(); Error
	const keyword cannot be reassign*/


/* FUNCTION SCOPING
	-Variable defined inside a function are not accessible / visible outside the function.
	-Variables declared with var, let, and const are quite similar when declared inside a function

	JAVASCRIPT VARIABLES HAS 3 TYPES OF SCOPE:
		1. Local / block scope
		2. Global scope
		3. Function scope */

console.log("------------------------");
console.log("Function Scooping");

{ //local variable
	let localVar = "Armando Perez";
	console.log(localVar);
}

// global
//console.log(localVar); // ERROR - not defined

let globalVar = "Mr. Worldwide";
console.log(globalVar);

{
	console.log(globalVar);
}

function showNames(){
	var functionVar = "Joe";
	const functionConst= "John";
	let functionLet = "Jane";

	console.log(functionVar);
	console.log(functionConst);
	console.log(functionLet);
};

showNames();

/*	ERROR - These are function scoped variable and cannot be accessed outside the function they were declared in. 
	console.log(functionVar);
	console.log(functionConst);
	console.log(functionLet);*/

/* NESTED FUNCTIONS
	-Creating another function inside a function */
	console.log("------------------------");
	console.log("NESTED FUNCTIONS");

	function myNewFunction(){
		let name = "Jane";

		function nestedFunction(){
			let nestedName = "John";
			console.log(name);
		};
		nestedFunction(); // result to not defined error
		console.log(nestedFunction);
	};

myNewFunction();

// GLOBAL SCOPED VARIABLE
console.log("------------------------");
console.log("GLOBAL SCOPED VARIABLE")

let globalName = "Zuitt";

function myNewFunction2(){
	let nameInside = "Renz";
	console.log(globalName);
};

myNewFunction2();

// ALERT -- allows us to show a small window at the top of our browser page to show info to users.

//alert ("DO NOT PROCEED");

function showSampleAlert() {
	alert("DO NOT PROCEED");
}

showSampleAlert ();
//alert messages inside a function will only execute whenever we call / invoke the function

console.log("Sample console log"); // will appear after the alert is dismissed

/* NOTES:
	1. Only show an alert for short dialogs/messages to the user.
	2. Do not overuse alert() because the program/js has to wait for it to be dismissed before continuing */


/* PROMPT
	prompt() allow us to show small window at the top of the browser to gather user input. */

	/*let samplePrompt = prompt("Enter your Full Name");
	console.log(samplePrompt);
	console.log(typeof samplePrompt); // string

	console.log("Hello, " + samplePrompt);
*/
	function printWelcomeMessage(){
		let firstName = prompt("Enter your first name: ");
		let lastName = prompt("Enter your last name: ");

		console.log("Hello, " + firstName + " " + lastName + "!");
		console.log("Welcome to my page!");
	}

	printWelcomeMessage();

/*[SECTION] FUNCTION NAMING CONVENTION
	-Function name should be definitive of the taask it will perform. It usually contains a verb.

	-Avoid generic names to avoid confusion within our code.
		get();

	-Avoid pointless and inappropiate function names. Example: foo, bar, etc.
		function foo();

	-Name your function in lowercase. Follow camelCase when naming variables and functions.
	*/

	function getCourses(){
		let courses = ["Science 101", "Math 101", "English 101"];
		console.log(courses);
	}
	getCourses();

	function displayCarInfo(){
		console.log("Brand: Toyota");
		console.log("Type: Sedan");
		console.log("Price: 1,500,000");
	}

	displayCarInfo();